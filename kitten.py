import sys
from kitty.boss import Boss
from pathlib import Path
import subprocess


# NOTE - this script runs using Kitty's embedded Python
# at time of writing Kitty v0.27.1 embedded Python v3.9.4
# You can call this script with

def notify(message: str) -> None:
    subprocess.check_call(['notify-send', message])


def main(args: list[str]):
    if args[-1] in ('db_lookup', 'db_reverse_lookup'):
        return subprocess.check_output(['xsel', '--primary'], encoding='utf8')


def handle_result(args: list[str], answer: str, target_window_id: int, boss: Boss) -> None:
    window = boss.window_id_map.get(target_window_id)
    if window is None:
        return

    root = Path(args[0]).parent
    try:
        if args[-1] == 'version':
            boss.call_remote_control(window, (
                'send-text',
                f"# Python version: {sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}\n"
            ))
        if args[-1] in ('db_lookup', 'db_reverse_lookup'):
            result = subprocess.run(
                [
                    str(root / '.venv' / 'bin' / 'python'),
                    str(root / 'pgtool.py'),
                    args[-1],
                    answer,
                ],
                encoding='utf8',
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            if result.returncode != 0:
                notify(f"ERROR {result.returncode}: {result.stderr}")
            else:
                window.paste_text(result.stdout)

    except Exception as e:
        notify(f"EXCEPTION: {e}")
