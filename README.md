## What's going on!?

In this repo there's `kitten.py` which is designed to run inside Kitty's embedded Python interpreter (with access to the
various bindings it provides). It's a very limited script since it only has access to sub weird subset of core.

Everything else in the repo is designed to be run in a virtual environment specifically for this project. `kitten.py`
invokes things from the repo as required.

## Setup

I'm using Python 3.10 at the moment, other versions may work but I haven't tried them.

```
python -mvenv venv
source venv/bin/activate
pip install -r requirements.txt
sudo apt install xsel rofi
```

In your kitty config (`~/.config/kitty/kitty.conf`) something like this:

```
map f1 kitten /path/to/this/repo/kitten.py db_lookup
map f2 kitten /path/to/this/repo/kitten.py db_reverse_lookup
```
