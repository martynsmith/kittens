from __future__ import annotations

import argparse
import sys
import psycopg2
from psycopg2 import sql
from psutil import Process
import subprocess


def parse_psql_commandline(cmdline: list[str]) -> dict[str, str]:
    cmdline.pop(0)  # Remove the command itself (psql)

    parsed = {"args": []}

    flags = {
        "-w",
        "--no-password",
        "-W",
        "--password",
        "-l",
        "--list",
        "-V",
        "--version",
        "-X",
        "--no-psqlrc",
        "-1",
        "--single-transaction",
        "-?",
        "--help",
        "--help=options",
        "--help=commands",
        "--help=variables",
        "-A",
        "--no-align",
        "--csv",
        "-H",
        "--html",
        "-t",
        "--tuples-only",
        "-x",
        "--expanded",
        "-z",
        "--field-separator-zero",
        "-0",
        "--record-separator-zero",
    }

    options = {
        "-c",
        "--command",
        "-d",
        "--dbname",
        "-f",
        "--file",
        "-v",
        "--set",
        "--variable",
        "-a",
        "--echo-all",
        "-b",
        "--echo-errors",
        "-e",
        "--echo-queries",
        "-E",
        "--echo-hidden",
        "-L",
        "--log-file",
        "-n",
        "--no-readline",
        "-o",
        "--output",
        "-q",
        "--quiet",
        "-s",
        "--single-step",
        "-S",
        "--single-line",
        "-F",
        "--field-separator",
        "-P",
        "--pset",
        "-R",
        "--record-separator",
        "-T",
        "--table-attr",
        "-h",
        "--host",
        "-p",
        "--port",
        "-U",
        "--username",
    }

    while cmdline:
        arg = cmdline.pop(0)
        if arg in flags:
            continue

        found_option = False
        for option in options:
            if arg == option:
                parsed[arg] = cmdline.pop(0)
                found_option = True
                break
            if arg.startswith(option):
                parsed[option] = arg.split('=', 1)[1]
                found_option = True
                break
        if not found_option:
            parsed['args'].append(arg)

    if len(parsed['args']) == 2:
        parsed['--username'] = parsed['args'][1]

    if len(parsed['args']) >= 1:
        parsed['--dbname'] = parsed['args'][0]

    if " " in parsed.get('--dbname', '') or "=" in parsed.get('--dbname', ''):
        del parsed['--dbname']

    connection_options = {}
    if '-p' in parsed:
        connection_options['port'] = parsed['-p']
    if '--port' in parsed:
        connection_options['port'] = parsed['--port']
    if '-h' in parsed:
        connection_options['host'] = parsed['-h']
    if '--host' in parsed:
        connection_options['host'] = parsed['--host']
    if '-d' in parsed:
        connection_options['dbname'] = parsed['-d']
    if '--dbname' in parsed:
        connection_options['dbname'] = parsed['--dbname']
    if '-U' in parsed:
        connection_options['user'] = parsed['-U']
    if '--username' in parsed:
        connection_options['user'] = parsed['--username']

    return connection_options


def get_postgres_connection_from_process(proc: Process) -> psycopg2.Connection:
    connect_options = dict(
        dbname="sharesies",
        user=None,
        password=None,
        host=None,
        port=None,
    )

    cmdline = proc.cmdline()
    environ = proc.environ()

    if 'PGDATABASE' in environ:
        connect_options['dbname'] = environ['PGDATABASE']
    if 'PGHOST' in environ:
        connect_options['host'] = environ['PGHOST']
    if 'PGPORT' in environ:
        connect_options['port'] = environ['PGPORT']
    if 'PGUSER' in environ:
        connect_options['user'] = environ['PGUSER']
    if 'PGPASSWORD' in environ:
        connect_options['password'] = environ['PGPASSWORD']

    for idx, arg in enumerate(cmdline):
        if arg == '-p':
            connect_options['port'] = cmdline[idx + 1]
        if arg == '-h':
            connect_options['host'] = cmdline[idx + 1]
        if arg == '-d':
            connect_options['dbname'] = cmdline[idx + 1]

    connect_options.update(parse_psql_commandline(cmdline))

    return psycopg2.connect(**connect_options)


def get_tables(cur: psycopg2.Cursor) -> set[str]:
    cur.execute('''
    SELECT table_name
    FROM information_schema.columns
    WHERE table_schema = 'public' AND column_name = 'id' AND data_type IN ('text', 'uuid')
    ''')
    return set(table_name for table_name, in cur)


def get_table_for_id(cur, id: str, tables: set[str]) -> tuple[str, str] | None:
    lookup_query = sql.SQL(
        ' UNION '.join(
            ["""SELECT {} AS table, 'id' AS column FROM {} WHERE id = %(id)s"""] * len(tables))).format(
        *[f for sublist in [(sql.Literal(table), sql.Identifier(table)) for table in tables] for f in sublist]
    )

    cur.execute(lookup_query, dict(id=id))
    match = None
    for table, column in cur:
        if table and column:
            match = (table, column)
    return match


def notify(message: str) -> None:
    subprocess.check_call(['notify-send', message])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="pgtool",
        description="Look up UUIDs in a database"
    )
    parser.add_argument('command', choices=("db_lookup", "db_reverse_lookup"), help="Command")
    parser.add_argument('uuid', help="UUID to look up")
    parser.add_argument('-p', '--pid', type=int, help="PID of the psql process (defaults to searching)")
    args = parser.parse_args()

    if args.pid:
        psql_process = Process(args.pid)

        if psql_process.name() != 'psql':
            psql_process = next((p for p in psql_process.children(recursive=True) if p.name() == 'psql'), None)

        assert psql_process and psql_process.name() == 'psql', "Process isn't a psql process"
    else:
        terminal_process = Process().parent()
        psql_process = next((p for p in terminal_process.children(recursive=True) if p.name() == 'psql'), None)

    if not psql_process:
        notify("Couldn't find running psql process in this shell")
        sys.exit(3)

    conn = get_postgres_connection_from_process(psql_process)
    with conn.cursor() as cur:
        cur.execute("SET SESSION lock_timeout = '5s'")
        cur.execute("SET SESSION statement_timeout = '10s'")

        tables = get_tables(cur)
        match = get_table_for_id(cur, args.uuid, tables)
        if not match:
            # No match
            sys.exit(4)

        if args.command == 'db_lookup':
            # This just queries the match directly
            print(
                sql.SQL("""SELECT * FROM {} WHERE {}={};""").format(
                    sql.Identifier(match[0]),
                    sql.Identifier(match[1]),
                    sql.Literal(args.uuid)
                ).as_string(conn),
                end=""
            )

        if args.command == "db_reverse_lookup":
            cur.execute("""
            SELECT
                s.relname AS source_table,
                (SELECT attname FROM pg_catalog.pg_attribute WHERE attrelid = s.oid AND attnum = ANY(r.conkey)) AS source_columns,
                t.relname AS target_table,
                (SELECT attname FROM pg_catalog.pg_attribute WHERE attrelid = t.oid AND attnum = ANY(r.confkey)) AS target_columns
            FROM
                pg_catalog.pg_constraint r
                JOIN pg_catalog.pg_class s ON r.conrelid = s.oid
                JOIN pg_catalog.pg_class t ON r.confrelid = t.oid
            WHERE
                r.contype = 'f' AND t.relname = %(table)s
            ORDER BY
                1, 2
            """, dict(table=match[0]))

            proc = subprocess.Popen(['rofi', '-dmenu'], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)

            rofi_input = b''
            for source_table, source_columns, target_table, target_columns in cur:
                rofi_input += f"{source_table}.{source_columns}\n".encode('ascii')

            out, _ = proc.communicate(rofi_input)
            out = out.decode('ascii').strip()
            if not out:
                sys.exit(5)

            table, column = out.split('.')

            print(
                sql.SQL("""SELECT * FROM {} WHERE {}={};""").format(
                    sql.Identifier(table),
                    sql.Identifier(column),
                    sql.Literal(args.uuid)
                ).as_string(conn)
            )
